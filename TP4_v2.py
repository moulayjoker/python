#! /usr/bin/env python3
import csv;
import os;

class PageMaker:    
    src = "";

    def __init__(self, source):
        self.src = source;

    #Génére le code source avec les valeurs dynamiques en parametre
    def genHtml (self, title, year, score):
        return """<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>De Niro Movies - """ + title + """</title>
</head>
<body>
<h1>Film: """ + title + """</h1>
<p>Année de sortie: """ + year + """</p>
<p>Score obtenue: """ + score + """</p>

</body>
</html>"""

    def genPage (self, dest, fname, code):
        #Contruction du chemin et sous répertoires !
        if not os.path.exists (dest) :
            os.makedirs (dest);

        #Création de la page
        f = open (dest + fname, "w");
        f.write (code);
        f.close ();


    def generate_html (self, url):
        with open(self.src, 'r') as csv_file:
            rows = csv.reader ( csv_file, delimiter=',') 
            next(rows); #saut de la première ligne
            for r in rows:
                #Génération de la page avec les options choisis
                self.genPage (url, r[0] + " -" + r[2] + " =" + r[1] + ".html", self.genHtml(r[2], r[0], r[1]));

pm=PageMaker ("deniro.csv");
pm.generate_html("temp/");