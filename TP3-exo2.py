#! /usr/bin/env python3
from csv import reader;

#Enregistrment du meilleur film de la liste
best = -1;
nameBest="";

#Période de recherche et nombre de dispo
pmin=2000;
pmax=2010;
nb=0;

#Nom du fichir de sortie 
sortie="deniro.txt";

#Déclaration de la procédure d'écriture dans le fichier
def writeDataFile (path, txt) :
    f = open (path, "w");
    f.write (txt);
    f.close ();

    #Petit message qui sera écrit
    print (txt);

# Ouvrir le fichier en lecture seule
f = open ('deniro.csv', "r");
ligne = reader (f);

for l in ligne :
    try :
        if pmin <= int (l[0]) <= pmax :
            nb+=1;

        if best < int (l[1]) :
            best = int (l[1]);
            nameBest = l[2];

    #Évite de lire gérer le bug de la première ligne et de toute valeur non numérique
    except (TypeError, ValueError) :
        print ("***** Il n'y a pas que de nombres ici ! *****");

f.close ();

#Controle s'il y a eu des films pour la période sélectionner
nbt = "Aucun film de la période séléctionné";
if nb > 0 :
    nbt="Il y a " + str (nb) + " films correspondant aux critères";

#Y a t'il un bon film ?
bestt = "Au royaume des aveugles, le borgne est roi ! Mais là, ce royaume est desertique !";
if best > -1 :
    bestt = "Le film qui a le meilleur score est " + nameBest [2:-1];

try:
    writeDataFile (sortie, nbt + "\n" + bestt);
except PermissionError:
    print ("***** Oups, tu n'es pas autorisé à écrire à la destionation déclarer ... Démerde toi avec ton CHMOD ! *****");
