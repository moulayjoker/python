#! /usr/bin/env python3
import os

src="flags";
dest="flagsbis";
ver="missing.png";
abrev=3;

try:
    #Contruction du chemin et sous répertoires !
    if not os.path.exists (dest) :
        os.makedirs (dest);

    #Variable de test du nom de fichier
    test = -1;

    for f in os.listdir (src):
        if f != ver :
            test = len (f);
            #La rechrche dans la liste d'un index négatif ne génére aucune erreur !
            if test > abrev + 4 :
                os.rename (src + "/" + f, dest + "/" + (f[:abrev]).upper () + ".png");
            elif test > 4:
                os.rename (src + "/" + f, dest + "/" + (f[:-4]).upper () + ".png");
            else :
                print ("Le fichier", f, "est sans nom !");
            print("[+] file %s moved" % f);
except:
    print ("Ça a merdé quelque part ... Démerde toi pour trouver où !");
