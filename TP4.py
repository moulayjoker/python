#! /usr/bin/env python3
import csv;

class PageMaker:    
    src="";

    def __init__(self, source):
        self.src = source;

    def gentext (year, title, score, dest):
        search_year = "[Year]"
        search_title = "[Title]"
        search_score = "[Score]"

    def generate_html (self, url):
        with open(self.src, 'r') as csv_file:
            rows = csv.reader(csv_file, delimiter=',') 
            lineCount = 0
            for r in rows:
                if lineCount != 0: # pas la ligne d'entête
                    with open(r'template.html', 'r') as file:
                        data = file.read ();
                        data = data.replace ("[Year]", r[0]);
                        data = data.replace ("[Title]", r[1]);
                        data = data.replace ("[Score]", r[2]);
                    fichier= url + r[0] + " - " + r[1] + " = " + r[2][2:-1] + ".html";
                    with open(r''+fichier, 'w') as file:
                        file.write(data)
                lineCount += 1



pm=PageMaker ("deniro.csv");
pm.generate_html("temp/");